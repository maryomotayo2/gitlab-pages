/*
Package metrics is the primary entrypoint into LabKit's metrics utilities.

Provided Functionality

- Provides http middlewares for recording metrics for an http server

*/
package metrics
